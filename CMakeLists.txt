cmake_minimum_required(VERSION 3.24)
project(pangotest)

enable_testing()

find_package(PkgConfig REQUIRED)
pkg_check_modules(pangotest_deps REQUIRED cairo pango pangocairo)

add_executable(pangotest pangotest.cpp)

target_link_libraries(pangotest PUBLIC ${pangotest_deps_LIBRARIES})
target_include_directories(pangotest PRIVATE ${pangotest_deps_INCLUDE_DIRS})

foreach(i RANGE 0 9 1)
  configure_file(dummy.txt ${i}/dummy.txt COPYONLY)
  message(STATUS ${pangotest_BINARY_DIR}/${i})
  
  foreach(j RANGE 0 9 1)
    add_test(
      NAME
        ${TestPrefix}pangotest-${i}-${j}
      COMMAND
        pangotest ${j} 0 ${pangotest_BINARY_DIR}/${i}
    )
  endforeach()
endforeach()

